<?php

/**
 * @file
 * Main file for preprocess custom theme hooks.
 */

/**
 * Implements template_preprocess_HOOK() for
 * buttons-content-previous-next.html.twig.
 */
function template_preprocess_buttons_content_previous_next(&$variables) {
  /* @var $entity \Drupal\Core\Entity\EntityInterface */
  $entity = $variables['entity'];
  $variables['next'] = [];
  $variables['previous'] = [];

  /* @var \Drupal\Core\Entity\EntityStorageInterface $entity_storage*/
  $entity_storage = Drupal::entityTypeManager()
    ->getStorage($entity->getEntityTypeId());

  // Getting next entity ID.
  $next_entity_id = $entity_storage->getQuery()
    ->condition('type', $entity->bundle())
    ->condition('created', $entity->getCreatedTime(), '>')
    ->range(0, 1)
    ->sort('created', 'ASC')
    ->execute();
  // Load next entity if its id exists.
  if (!empty($next_entity_id)) {
    $next_entity = $entity_storage->load(array_shift($next_entity_id));
    // Set the label and url to entity.
    $variables['next']['entity'] = $next_entity;
    $variables['next']['label'] = $next_entity->label();
    $variables['next']['url'] = $next_entity->toUrl()
      ->toString(TRUE)
      ->getGeneratedUrl();
  }

  // Getting previous entity ID.
  $previous_entity_id = $entity_storage->getQuery()
    ->condition('type', $entity->bundle())
    ->condition('created', $entity->getCreatedTime(), '<')
    ->range(0, 1)
    ->sort('created', 'DESC')
    ->execute();
  // Load next entity if its id exists.
  if (!empty($previous_entity_id)) {
    $previous_entity = $entity_storage->load(array_shift($previous_entity_id));
    // Set the label and url to entity.
    $variables['previous']['entity'] = $previous_entity;
    $variables['previous']['label'] = $previous_entity->label();
    $variables['previous']['url'] = $previous_entity->toUrl()
      ->toString(TRUE)
      ->getGeneratedUrl();
  }
}
