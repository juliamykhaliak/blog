<?php

namespace Drupal\dlog\Plugin\DlogHero\Path;

use Drupal\dlog_hero\Plugin\DlogHero\Path\DlogHeroPathPluginBase;
use Drupal\media\MediaInterface;

/**
 * Hero block for path.
 *
 * @DlogHeroPath(
 *   id = "dlog_blog",
 *   match_type = "listed",
 *   match_path = {"/blog"},
 *   weight = 100,
 * )
 */
class DlogBlog extends DlogHeroPathPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeroSubtitle() {
    return 'Molestie a iaculis at erat pellentesque adipiscing commodo elit at.
     Consequat semper viverra nam libero justo laoreet sit';
  }

  /**
   * {@inheritdoc}
   */
  public function getHeroImage() {
    /** @var \Drupal\media\MediaStorage $media_storage */
    $media_storage = $this->getEntityTypeManager()->getStorage('media');
    $media_image = $media_storage->load(22);
    if ($media_image instanceof MediaInterface) {
      return $media_image->get('field_media_image')->entity->get('uri')->value;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getHeroVideo() {
    $media_storage = $this->getEntityTypeManager()->getStorage('media');
    $media_video = $media_storage->load(21);
    if ($media_video instanceof MediaInterface) {
      return [
        'video/mp4' => $media_video->get('field_media_video_file')->entity->get('uri')->value,
      ];
    }
  }

}
